package oauth2.samples.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import javax.sql.DataSource;
import java.util.Arrays;

/**
 * @author chai
 * @since 2020/12/2
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

//    @Autowired
//    ClientDetailsService clientDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RedisConnectionFactory redisConnectionFactory;
    @Autowired
    DataSource dataSource;
    @Autowired
    CustomAdditionalInformation customAdditionalInformation;

    /** Jwt密钥 */
    final String SIGNING_KEY = "ddnYGD";

    @Bean
    TokenStore tokenStore() {
        // token存储位置
//        return new InMemoryTokenStore();
//        return new RedisTokenStore(redisConnectionFactory);
        // 使用jwt作为token 无需在服务端进行存储
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        converter.setJwtClaimsSetVerifier((claims -> {
            // 这里添加 Jwt 中保存的自定义信息
            claims.put("author", "33333");
        }));
        return converter;
    }

    @Bean
    AuthorizationServerTokenServices tokenServices() {
        // 配置token的基本信息 如存储位置、有效期、RefreshToken等
        DefaultTokenServices services = new DefaultTokenServices();
        services.setClientDetailsService(clientDetailsService());
        services.setTokenStore(tokenStore());
        services.setSupportRefreshToken(true);
        // 添加Jwt处理链
        TokenEnhancerChain chain = new TokenEnhancerChain();
        chain.setTokenEnhancers(Arrays.asList(jwtAccessTokenConverter(), customAdditionalInformation));
        services.setTokenEnhancer(chain);
        // 2小时 此配置可配置在数据库中
//        services.setAccessTokenValiditySeconds(60 * 60 *2);
        // 3天
//        services.setRefreshTokenValiditySeconds(60 * 60 * 24 * 3);
        return services;
    }

    @Bean
    ClientDetailsService clientDetailsService() {
        // 客户端信息存储地址
        return new JdbcClientDetailsService(dataSource);
    }

    @Bean
    AuthorizationCodeServices authorizationCodeServices() {
        // 授权码的存储位置
        return new InMemoryAuthorizationCodeServices();
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.checkTokenAccess("permitAll()")
                .allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // 配置注册客户端的基本信息
//        clients.inMemory()
//                .withClient("OAuth2-Samples")
//                .secret(passwordEncoder.encode("c9cd6cfb-3a82-41ff-875b-10206d049c97"))
//                .resourceIds("res1")
//                .scopes("all")
//                .authorizedGrantTypes("authorization_code","refresh_token")
//                .redirectUris("http://127.0.0.1:8082/index.html");
        clients.withClientDetails(clientDetailsService());
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authorizationCodeServices(authorizationCodeServices())
                .tokenServices(tokenServices());
    }
}
