package oauth2.samples.clientserver.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Map;

/**
 * @author chai
 * @since 2020/12/4
 */
@Component
@SessionScope
public class TokenTask {

    private String access_token = "";
    private String refresh_token = "";

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 请求access_token
     * @param code
     * @return
     */
    public String getData(String code) {
        if ("".equals(access_token) && code != null) {
            // 授权服务器重定向到此页面时
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("code", code);
            map.add("client_id", "OAuth2-Samples");
            map.add("client_secret", "c9cd6cfb-3a82-41ff-875b-10206d049c97");
            map.add("redirect_uri", "http://127.0.0.1:8082/index.html");
            map.add("grant_type", "authorization_code");
            Map<String, String> resp = restTemplate.postForObject("http://127.0.0.1:8080/oauth/token", map, Map.class);
            System.out.println(resp);
            access_token = resp.get("access_token");
            refresh_token = resp.get("refresh_token");
        }
        return loadDataFormResServer();
    }

    /**
     * 请求用户资源
     * @return
     */
    public String loadDataFormResServer() {
        if ("".equals(access_token)) {
            return null;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer" + access_token);
        HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(headers);
        // 获取用户的详细信息
        ResponseEntity<String> entity = restTemplate.exchange("http://127.0.0.1:8081/admin/hello/", HttpMethod.GET, httpEntity, String.class);
        return entity.getBody();
    }

    /**
     * 定时更新 access_token 与 refresh_token
     */
//    @Scheduled(cron = "")
    public void refreshToken() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", "OAuth2-Samples");
        map.add("client_secret", "c9cd6cfb-3a82-41ff-875b-10206d049c97");
        map.add("grant_type", "refresh_token");
        map.add("refresh_token", refresh_token);
        Map<String, String> resp = restTemplate.postForObject("http://127.0.0.1:8080/oauth/token", map, Map.class);
        access_token = resp.get("access_token");
        refresh_token = resp.get("refresh_token");
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }
}
